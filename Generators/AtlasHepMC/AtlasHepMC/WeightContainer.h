/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
   Author: Andrii Verbytskyi andrii.verbytskyi@mpp.mpg.de
*/
#ifndef ATLASHEPMC_WEIGHTCONTAINER_H
#define ATLASHEPMC_WEIGHTCONTAINER_H
#include "HepMC/WeightContainer.h"
#endif
